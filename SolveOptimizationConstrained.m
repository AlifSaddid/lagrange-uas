% Todo: Coba coba 
function [] = SolveOptimizationConstrained()
  p = [1; 1; 1];
  TOL = [0; 0; 0];
  step = 0;
  for i = 1 : 50
    H = Hessian(p(1), p(2), p(3));
    grad = -Grad(p(1), p(2), p(3));
    fprintf("(%f, %f) Lambda = %f\n", p(1), p(2), p(3));  
    
    v = H \ grad;
    p = p + v;
    step += 1;
  end
endfunction

function grad = Grad(x, y, l)
  grad = [
    10*l + (225 * y^(1/4))/(2 * x^(1/4));
    4*l + (75 * x^(3/4))/(2 * y^(3/4));
    10*x + 4*y - 1500
  ];
endfunction

function hessian = Hessian(x, y, l)
  hessian = [
    -(225*y^(1/4))/(8*x^(5/4)),  225/(8*x^(1/4)*y^(3/4)), 10;
    225/(8*x^(1/4)*y^(3/4)) , -(225*x^(3/4))/(8*y^(7/4)), 4;
    10, 4, 0;  
  ];
endfunction
